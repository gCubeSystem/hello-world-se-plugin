This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Hello World Smart Executor Plugin


## [v2.0.0]

- Ported plugin to smart-executor APIs 3.0.0 [#21619]
- Switched smart-executor JSON management to gcube-jackson [#19647]


## [v1.0.0]

- First Release. The component was migrated from smart-executor-hello-world-plugin

